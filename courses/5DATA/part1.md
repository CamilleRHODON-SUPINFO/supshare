---
title: "5DATA - Part 2 - Labs"
author: 
date: '2018-20-11'
tags:
  - Labs
---

We want to model an application (very simplified) for the management of an online music service, with the data model below:
Artist ---writes--> Song ---order---> Playlist
An artist writes a song and the songs are organized in playlists.

## Question 1

Propose the corresponding relational schema (Tables with SQL statements for creating tables)

>### Database Struct
>
>![Database Struct Diagram](https://i.imgur.com/QutaZ2S.png "Database Struct Diagram")

>### Artist
>
>_id|Name
>---|---
> 0 | Vald
> 1 | Daft Punk
> 2 | Stupeflip

>### Song
>
>_id|Name|_artistId
>---|---|---
> 0 | NQTMQMQMB (avec Suikon Blaz AD)|0
> 1 | Pensionman|0
> 2 | J fume pu d'shit|2
> 3 | Harder Better Faster Stronger|1
> 4 | Get Lucky (feat Pharell Williams)|1

>### Playlist
>
>_id|Name
>---|---|---
> 0 | Good music
> 1 | Shitty music

>### PlaylistMembership
>
>_id|_songId|_playlistId
>---|---|---
> 0 | 0 | 0
> 1 | 1 | 0
> 2 | 2 | 0
> 3 | 3 | 0
> 4 | 4 | 1

## Question 2

Propose a Cassandra schema (This one is based on the access patterns, ie the requests that one has to realize).

Here are the two access patterns considered:

Find all song titles

Find all songs titles of a particular playlist

Which of these access patterns can potentially present a problem with a relational system in a BigData context and why?

You can give the corresponding SQL queries to explain and justify your answer.

>### AllSongs
>
>```SQL
>CREATE TABLE AllSongs(
>    _songId int,
>    SongName text,
>    Artist text,
>    PRIMARY key(_songId, SongName));
>```
>
>_songId|SongName|Artist
>---|---|---
> 0 | NQTMQMQMB (avec Suikon Blaz AD) | Vald
> 1 | Pensionman | Vald
> 2 | J fume pu d'shit | Stupeflip
> 3 | Harder Better Faster Stronger | Daft Punk
> 4 | Get Lucky (feat Pharell Williams) | Daft Punk

>### SongsByPlaylist
>
>```SQL
>CREATE TABLE SongsByPlaylist(
>    _playlistId int,
>    PlayListName text,
>    SongName text,
>    Artist text,
>    PRIMARY key(_playlistId, SongName));
>```
>
>_playlistId|PlayListName|SongName|Artist
>---|---|---|---
> 0 | PlayList1 | NQTMQMQMB (avec Suikon Blaz AD) | Vald
> 1 | PlayList2 | Pensionman | Vald
> 0 | PlayList1 | J fume pu d'shit | Stupeflip
>>> 1 | PlayList2 | NQTMQMQMB (avec Suikon Blaz AD) | Vald
> 0 | PlayList1 | Harder Better Faster Stronger | Daft Punk
>This database struct is way more effective when you're looking to search all the songs of a playlist because you will use a query targeting only one table. To compare, in the RDB struct you'll have to retreive the data and make a JOIN between three tables.

>### RDB query
>
>```SQL
>SELECT 
>    p.Name AS "Playlist Name",
>    s.Name AS "Song Name"
>FROM Playlist as p
>INNER JOIN PlaylistMembership as pm ON pm._playlit = p._id
>LEFT JOIN Song as s ON pm._songId = s._id
>WHERE p._id = 0
>```

>### Cassandra query
>
>```SQL
>SELECT 
>    PlayListName AS "Playlist Name",
>    SongName AS "Song Name"
>FROM SongsByPlaylist
>WHERE p._id = 0
>```

## Question 3

>Cassandra suggests in this case the creation of a single table.
>```SQL
>CREATE TABLE playlists (
>    id_playlist int,
>    creator text,
>    song_order int,
>    song_id int,
>    title text,
>    lyrics text,
>    name text,
>    age int,
>  PRIMARY KEY  (id_playlist, song_order ) );
>```

Present advantages and disadvantages by answering the following questions:

- How many insertions (at worst) are needed to add a song to a playlist, in relational and in Cassandra?
>In the worst case you can have to do 4 insertions (Song, Artist, Playlist and PlaylistMembership) in the rdb model if the playlist, the artist the song are not yet referenced inside the db.

> In comparison, in the Cassandra model you'll always have only 1 insertion because all the data are store on only 1 row of 1 table (SongsByPlaylist).
- What can be said about queries that display a playlist, respectively in relational and in Cassandra (give the query if necessary)?
> The queries are way more simple with cassandra because you only have to trigger 1 table.

> In RDB you have to trigger 4 table, so make 3 JOIN and everyone knows JOIN are the AIDS of SQL.
- How many lines do I have to update when an artist's age changes, in relationship and in Cassandra?
> For Cassandra with the model we create before, you have to update 2 table (Songs and SongsByPlaylist) because all of th raw data are store inside each table.

> In RDB model you only have to update 1 table because (Artist) because data are chained relationaly (no shit).
- Conclusion?
> In conclusion, the RDB model (this game)is still relevant(god dammit) when the updating speed and consistency across the cluster is a key criteria of your app but in the case you main focus are reading speed and insertion speed Cassandra DB is a good solution.

## Question 4

NB. you notice that the identifier of the table is composite (compound) : (id_playlist, song_order ). 

Definition of compound Id:

>“A compound primary key consists of the partition key and the clustering key. The partition key determines which node stores the data. Rows for a partition key are stored in order based on the clustering key.“

Based on this explanation, which statements are true among the following:

1. A song is stored on a single server (true / false)?
2. The songs of the same playlist are all on one  server (true / false)?
3. The songs stored on a server are sorted on their identifier (true / false)?
4. The songs of the same playlist are stored one after the other (true / false)?

## Queston 5

Make a small drawing illustrating the characteristics of the storage of playlists in a distributed Cassandra system.

> Lol nop

## Question 6

Which access patterns can be evaluated very efficiently with this data organization?
> The Cassandra pattern is very effectibe in this orgarnisation because of the only need specified is to quickly access not quicl update.

which poses potential problems of coherence?
> The Cassandra pattern have obviously a lack of coherence between tables if you don't update carfully the data in every location it's stored.