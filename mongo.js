const MongoClient = require('mongodb').MongoClient;
//const assert = require('assert');

const user = encodeURIComponent('admin');
const password = encodeURIComponent('Srythuutr41');
const authMechanism = 'DEFAULT';

const url = `mongodb+srv://${user}:${password}@cluster0-3fyb7.mongodb.net/?authMechanism=${authMechanism}`;
var _db;

module.exports = {
	connect: () => {
		MongoClient.connect( url, async (err, client) => {
			_db =  client.db('forumdb');
		});
  	},
	getDb: () => {
		return _db;
	}
}

/*
// Create a new MongoClient
const client = new MongoClient(url);

// Use connect method to connect to the Server
client.connect(function(err) {
  assert.equal(null, err);
  console.log("Connected correctly to server");

  client.close();
}); */