"use strict";

const express =   require('express');
const routes =   require('./routes');
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 8080;

class Server {
    constructor() {
        this.app = express();
        this.app.use(express.static(__dirname + '/public'));
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use('/', routes);     
        this.app.listen(PORT);
    }
}

var server = new Server();
