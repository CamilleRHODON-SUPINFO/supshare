"use strict";
const express   	= require('express');
const bodyParser 	= require('body-parser');
const fs            = require('fs');
const showdown      = require('showdown');
const db =  require('./mongo');
db.connect();

//const CryptoJS 		= require('crypto-js');
//const crypto 		= require('crypto');
const router    	= express.Router();
//const jwtUtils = require('./utils/jwt');

router.get('/index', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});

router.get('/fichier/:part', (req, res) => {
    //res.render(__dirname + '/views/login.pug', {title:"Login"});
    const part = req.params.part;
    const converter = new showdown.Converter();
    
    fs.readFile(`./courses/${part}.md`, 'utf8', (err, data) => {
        if(err) res.send('{ success: false, reason: file does not exists}');
        res.send(converter.makeHtml(data));
    });
});

router.get('/comments/:id', async (req, res) => {
    const part = req.params.id;
    var _db = db.getDb();

    const messages = await _db.collection("messages").find({ articleId: { $eq: part } }).toArray((err, data) => {
        //if (err) throw err;
        res.send(data);
    });
});

router.get('/courses', (req, res) => {
    var _db = db.getDb();
    fs.readdir('./courses', (err, data) => {
        data.forEach( (o, i, a) => { a[i] = a[i].split('.')[0] });
        res.send(data)
    });
});


router.post('/login', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
});


router.post('/register', (req, res) => {
    const username = req.body.username;
    const email = req.body.email;
    const password = req.body.password;
    const confirmpassword = req.body.confirmpassword;
});



module.exports = router;


